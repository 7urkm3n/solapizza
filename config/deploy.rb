# config valid only for current version of Capistrano
lock "3.8.0"

set :application, "solapizza"
set :repo_url, "git@bitbucket.org:7urkm3n/solapizza.git"

set :deploy_to, '/home/deploy/solapizza'

set :rvm_type, :user
set :rvm_ruby_version, '2.2.3'
# set :console_user, 'deploy'

# For -  cap production RAILS:CONSOLE!
set :rails_env, "production"
set :deploy_via, :remote_cache

append :linked_files, "config/database.yml", "config/secrets.yml"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", "public/system", "public/uploads"

# after 'deploy:assets:precompile', 'deploy:assets:install'


namespace :deploy do

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end


namespace :rails do
  desc "Remote console"
  task :console do
    # run_interactively "bundle exec rails console #{rails_env}"
    # execute_interactively "RAILS_ENV=production rails c"
    exec "#{current_path}/bundle exec RAILS_ENV=production rails console"
  end
end


# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
