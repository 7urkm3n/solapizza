class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :title
      t.string :desc
      t.string :price

      t.timestamps null: false
    end
  end
end
