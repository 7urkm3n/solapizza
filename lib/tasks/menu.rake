namespace :menu do
  desc "TODO"

  task coupons: :environment do
    Coupon.create(title: 'Sandwich Deal', desc: 'Any 2 Whole 16" Sandwiches', price: "19.99")
  	Coupon.create(title: 'Pizza & Sandwich', desc: 'Medium Cheese Pizza & 16" Sandwich', price: '19.99')
  	Coupon.create(title: 'Steel City Special', desc: 'Large 1 Topping Pizza, Breadsticks & 2 Liter Soda', price: '21.99')
  	Coupon.create(title: 'Pittsburgh Special', desc: 'Large Cheese Pizza, Chesee Breadsticks & 10 Wings', price: '24.99')
  	Coupon.create(title: 'Sandwich & Wings', desc: 'Any Whole 16" Sandwich & 10 Wings', price: '18.99')
  	Coupon.create(title: 'Party Wings', desc: '30 Piece Wings', price: '22.99')
  	Coupon.create(title: '2 Medium Special', desc: '2 Medium 14" Cheese Pizzas', price: '19.99')
  	Coupon.create(title: '2 Large Special', desc: '2 Large 18" Cheese Pizzas', price: '24.99')
  	Coupon.create(title: 'Pizza & Wings', desc: 'Medium Cheese Pizza & 10 Wings', price: '18.99')
  	Coupon.create(title: '1 Topping Specail', desc: 'Large 1 Topping Pizza', price: '14.99')
  	Coupon.create(title: 'Gyro Specail', desc: '2 Gyros & Fries', price: '11.99')
  	Coupon.create(title: 'Fried Chicken Specail', desc: '6pc. Fried Chicken, 1 Order of Fires & 20 oz. Soda', price: '9.99')
  	Coupon.create(title: '2 Topping Specail', desc: 'Medium 2 Topping Pizza', price: '12.99')
  	Coupon.create(title: 'College Specail', desc: 'Large Cheese Pizza & 2 LIter Soda', price: '14.99')
  end

  task categories: :environment do
  	Category.create(title: 'Appetizers')
  	Category.create(title: 'Salads')
  	Category.create(title: 'Strombolis')
  	Category.create(title: 'NY Style Pizza')
  	Category.create(title: 'Red Pizza')
  	Category.create(title: 'White Pizza')
  	Category.create(title: 'Famous Pizza')
  	Category.create(title: 'Gluten-Free')
  	Category.create(title: 'Sandwiches')
  	Category.create(title: 'Gyros')
  	Category.create(title: 'Wings')
  	Category.create(title: 'Deserts')
  	Category.create(title: 'Beverages')
  end

end
