// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require js/jquery-migrate-1.1.1
//= require js/jquery.equalheights
//= require js/jquery.ui.totop
//= require js/TMForm.js
//= require js/jquery.tabs.min
//= require js/touchTouch.jquery
//= require js/jquery.easing.1.3
//require turbolinks
//require_tree .



