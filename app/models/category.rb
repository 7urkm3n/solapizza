class Category < ActiveRecord::Base
	# before_save { self.title = title.capitalize }
	has_many :products, dependent: :destroy
end
