json.array!(@coupons) do |coupon|
  json.extract! coupon, :id, :title, :desc, :price
  json.url coupon_url(coupon, format: :json)
end
