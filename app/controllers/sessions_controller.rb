class SessionsController < ApplicationController

	def admin
		@disable_nav = true
	end

  	def create
  		# render plain: params
		@user = User.find_by(email: params[:login][:email].downcase)
		if @user && @user.authenticate(params[:login][:password])
			log_in(@user)
			flash[:success] = "#{@user.email}, Successfully Logged In"
			redirect_to dashboard_path
		else
			flash.now[:danger] = "Incorrect User/Password"
			render 'sessions/admin'
		end
	end

	def destroy
		log_out if logged_in?
		redirect_to root_path
	end
end
