class CouponsController < ApplicationController
  before_action :set_coupon, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user
  
  def index
    @coupons = Coupon.all
  end

  def show
  end

  def new
    @coupon = Coupon.new
  end

  # GET /coupons/1/edit
  def edit
  end

  def create
    @coupon = Coupon.new(coupon_params)

    respond_to do |format|
      if @coupon.save
        format.html { redirect_to @coupon, notice: 'Coupon was successfully created.' }
        format.js {}
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @coupon.update(coupon_params)
        format.html { redirect_to @coupon, notice: 'Coupon was successfully updated.' }
        format.js {}
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @coupon.destroy
    respond_to do |format|
      # format.html { redirect_to coupons_url, notice: 'Coupon was successfully destroyed.' }
      format.js {}
      # format.js { render partial: false, :number => 1234123123, :locals => {:id => 123456564635242364} }
      # format.js {render :json => { feed_counts: "COUNTED" }, :status => :ok}
      # format.js { render :partial => "day" }
      # format.js { render "action", :locals => {:id => params[:id]} }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_coupon
      @coupon = Coupon.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def coupon_params
      params.require(:coupon).permit(:title, :desc, :price)
    end
end
