class StaticPagesController < ApplicationController
  before_action :title

  def index
    @coupons = Coupon.limit(14)
    @meta_title = "Sola Best Pittsburgh Pizza - Fast Free Delivery"
    @meta_description = "#{@title}, Best pizza in Pittsburgh, pizza, fried chicken, hoagie, salad, wings, fresh gyro, fish and huge slices. Cheap Lunch orders. Fast and Free delivery."
    @base = request.original_url
  end

  def about
  end

  def contacts
    @meta_title = "Contacts | #{@title}"
    @meta_description = "#{@title} working hours, contact numbers 412-481-3888 and location address. Open late night for free delivery"
    @base = request.original_url
  end

  def order_now
    @meta_title = "Order Now | #{@title}"
    @meta_description = "#{@title} takes online order with fast and free delivery in Pittsburgh area. 15203 Southside, 15210, 15211, 15213 Oakland, 15216, 15219 Uptown, 15222 Downtown, 15226, 15227. Open late night"
    @base = request.original_url
    # response.headers.delete "X-Frame-Options"
  end

  def menu
    @categories = Category.includes(:products)
    categories = ''
    @categories.each {|c| categories += c.title + ", "}
    
    @meta_title = "Menu | #{@title}"
    @meta_description = "Pittsburgh best #{categories} with Free and fast delivery."
    @base = request.original_url
  end

  def coupons
    @coupons = Coupon.all
    coupons = ''
    @coupons.each {|c| coupons += c.title + ", "}
    
    @meta_title = "Coupons | #{@title}"
    @meta_description = "Pittsburgh Best #{@title} deals and coupons. #{coupons}"
    @base = request.original_url
  end

  def gallery
    @meta_title = "Gallery"
  end

private
  
  def title
      @title = 'Sola Pizza'
  end
  
end
