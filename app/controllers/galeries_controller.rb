class GaleriesController < ApplicationController
  before_action :set_galery, only: [:show, :edit, :update, :destroy]

  def index
    @galeries = Galery.all
  end

  def new
    @galery = Galery.new
  end

  def edit
  end

  def create
    @galery = Galery.new(galery_params)

    respond_to do |format|
      if @galery.save
        format.html { redirect_to :back, notice: 'Galery was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @galery.update(galery_params)
        format.html { redirect_to :back, notice: 'Galery was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @galery.destroy
    respond_to do |format|
      format.js {}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_galery
      @galery = Galery.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def galery_params
      params.require(:galery).permit(:title, :image, :remote_image_url)
    end
end
