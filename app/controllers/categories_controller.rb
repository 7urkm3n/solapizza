class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user

  def index
    @category = Category.new
    @categories = Category.all
  end

  def show
    @products = Product.where(category_id: @category.id)
  end

  def new
    @category = Category.new
  end

  def edit
  end

  def create
    @category = Category.new(category_params)
    
    respond_to do |format|
      if @category.save
        # format.html { redirect_to @category, notice: 'Category was successfully created.' }
        flash[:success] = "#{@category.title} Successfully Added"
        format.js
      else
        format.html { render :new }
      end
    end
  end

  def update
    respond_to do |format|
      if @category.update(category_params)
        # format.html { redirect_to @category, notice: 'Category was successfully updated.' }
        format.js
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @category.destroy
    respond_to do |format|
      # format.html { redirect_to categories_url, notice: 'Category was successfully destroyed.' }
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_params
      params.require(:category).permit(:title)
    end
end
