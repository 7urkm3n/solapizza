class AdminController < ApplicationController
	before_action :logged_in_user

  def dashboard
    @product = Product.new
  	@categories = Category.all
  	@coupons = Coupon.all
  	# render plain: params
    
    if params[:category].blank?
      @products = Product.limit(15)
    else
      # @category_id = Category.order(:title).find_by(title: params[:category]).id
      @category = Category.order(:title).find_by(title: params[:category])
      @products = Product.where(category_id: @category.id)
    end
  end

end
