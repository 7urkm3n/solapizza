class UsersController < ApplicationController
  
  def index
  end

  def edit
  end

  def create
  	# render plain: params
    @user = User.new(user_params)
    # respond_to do |format|
      if @user.save
        redirect_to 'admin/dashboard'
        # format.html { redirect_to 'admin/dashboard'}
        # format.js {}
      else
        # format.html { render 'static_pages/index'}
        render 'sessions/admin'
      end
    # end
  end

  def destroy
  end

private

	def user_params
      params.require(:user).permit(:first_name, :last_name, :phone, :email, :password)
    end
end
